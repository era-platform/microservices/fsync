%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 02.08.2021

%% ====================================================================
%% Types
%% ====================================================================
   
%% ====================================================================
%% Constants and defaults
%% ====================================================================

%% ====================================================================
%% Define modules
%% ====================================================================

-define(BASICLIB, basiclib).
-define(FSYNCLIB, fsynclib).
-define(PLATFORMLIB, platformlib).

-define(APP, fsync).
-define(MsvcType, <<"fsync">>).

-define(SUPV, fsync_supv).
-define(CFG, fsync_config).

-define(DomainTreeCallback, fsync_domaintree_callback).

-define(DSUPV, fsync_domain_supv).
-define(DSRV, fsync_domain_srv).
-define(DU, fsync_domain_utils).

%% ------
%% From basiclib
%% ------
-define(BU, basiclib_utils).
-define(BLmulticall, basiclib_multicall).
-define(BLstore, basiclib_store).
-define(BLmonitor, basiclib_monitor_srv).
-define(BLlog, basiclib_log).

-define(BLfile, basiclib_wrapper_file).
-define(BLfilelib, basiclib_wrapper_filelib).
-define(BLfilecopier, basiclib_file_copier_srv).

%% From platformlib
-define(PCFG, platformlib_config).

-define(GLOBAL, platformlib_globalnames_global).
-define(GN_NAMING, platformlib_globalnames_naming).
-define(GN_REG, platformlib_globalnames_registrar).

-define(DTREE_SUPV, platformlib_domaintree_supv).

%% ====================================================================
%% Define logs
%% ====================================================================

-define(LOGFILE, {fsync,?APP}).

-define(LOG(Level,Fmt,Args), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(LOG(Level,Text), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), Text)).

-define(OUT(Level,Fmt,Args), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(OUT(Level,Text), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), Text)).

%% ====================================================================
%% Define other
%% ====================================================================
