%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 02.08.2021
%%% @doc

-module(fsync_domain_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    build_path/1,
    get_nodes/1,
    get_groups/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

build_path(shared) ->
    RootPath = ?CFG:sync_folder_path(),
    filename:join([?BU:to_list(RootPath), "shared"]);
build_path(Domain) ->
    RootPath = ?CFG:sync_folder_path(),
    filename:join([?BU:to_list(RootPath), "domains", ?BU:to_list(Domain)]).

%% TODO: when cross-site microservice would be implemented then it's server fsync sample should include in nodes CSM servers of other sites where domain is serviced.
%% TODO: shared
get_nodes(_Domain) ->
    NodesData = ?PCFG:get_nodes_data_by_msvc(?MsvcType),
    SNs = lists:map(fun(Data) -> {maps:get(<<"server">>,Data),maps:get(<<"node">>,Data)} end, NodesData),
    element(2,lists:unzip(lists:ukeysort(1,SNs))).

%% TODO: when cross-site microservice would be implemented then it's server fsync sample should define groups where local site and other sites CSM are separated.
%% TODO: shared
get_groups(_Domain) -> [].


%% ====================================================================
%% Internal functions
%% ====================================================================