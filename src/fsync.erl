%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 02.08.2021
%%% @doc Folder synchronization cross-node, cross-server, cross-site, per-domain
%%%   Application could be setup by application:set_env:
%%%      log_destination
%%%          descriptor to define log file folder and prefix.
%%%          Default: {'fsync','erl'}
%%%      sync_folder_path
%%%          where sync folder path is located. Domain sync folders would be located inside it's path.
%%%          Default: CWD/sync_folder
%%%      max_user_watches
%%%          how many user watches for inotify could be used totally on server (put this value into /proc/sys/fs/inotify/max_user_watches 
%%%          Default: 10000000
%%%      cooldown_timeout
%%%          events from file system are queued and collected for this timeout in ms to build and shrink groups.
%%%          Default: 1000
%%%      history_deleted_keep_days
%%%          after file is deleted it would keep in history to avoid download. After this timeout (in days) it should be deleted from history.
%%%          Default: 365
%%%      resync_interval
%%%          interval of sync-check with all provided nodes.
%%%          Default: 60000
%%%      resync_immediately_after_fsync_result
%%%          if extraordinary sync should start after 1 sec after previous sync finished with non-zero changes.
%%%          Default: true
%%%      force_sync_node
%%%          if extraordinary node sync should start after it send get_sync_info request with different hashcode.
%%%          Default: true
%%%      copier_bandwidth
%%%          Bandwidth limit for downloader (Bytes/s). Adds pauses inside download process to provide less speed.
%%%          Default: 65536000
                         
-module(fsync).
-author('Peter <tbotc@yandex.ru>').

-behaviour(application).

-export([
    start/0,
    update/0
]).

-export([
    start/2,
    stop/1
]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public
%% ====================================================================

%% -------------------------------------------
%% Start method
%% -------------------------------------------
-spec start() -> ok | {error,Reason::term()} | {retry_after,Timeout::non_neg_integer(),Reason::term()}.
%% -------------------------------------------
start() ->
    case application:ensure_all_started(?APP, permanent) of
        {ok, _Started} -> ok;
        Error -> Error
    end.

%% -------------------------------------------
%% Update options
%% -------------------------------------------
-spec update() -> ok.
%% -------------------------------------------
update() ->
    setup_dependencies(),
    ok.

%% ===================================================================
%% Callback functions (application)
%% ===================================================================

%% -------------------------------------
%% @doc Starts application
%% -------------------------------------
start(_, _) ->
    ?OUT('$info',"~ts. ~p start", [?APP, self()]),
    setup_dependencies(),
    ensure_deps_started(),
    ?SUPV:start_link(undefined).

%% -------------------------------------
%% @doc Stops application.
%% -------------------------------------
stop(State) ->
    ?OUT('$info',"~ts: Application stopped (~120p)", [?APP, State]),
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

%% @private
setup_dependencies() ->
    % fsynclib
    ?BU:set_env(?FSYNCLIB, 'log_destination', {fsync,erl}),
    ?BU:set_env(?FSYNCLIB, 'max_user_watches', ?CFG:max_user_watches(10000000)),
    ?BU:set_env(?FSYNCLIB, 'cooldown_timeout', ?CFG:cooldown_timeout()),
    ?BU:set_env(?FSYNCLIB, 'history_deleted_keep_days', ?CFG:history_deleted_keep_days()),
    ?BU:set_env(?FSYNCLIB, 'resync_interval', ?CFG:resync_interval()),
    ?BU:set_env(?FSYNCLIB, 'resync_immediately_after_fsync_result', ?CFG:resync_immediately_after_fsync_result()),
    ?BU:set_env(?FSYNCLIB, 'force_sync_node', ?CFG:force_sync_node()),
    ?BU:set_env(?FSYNCLIB, 'copier_bandwidth', ?CFG:copier_bandwidth()),
    ok.

%% --------------------------------------
%% @private
%% starts deps applications, that out of app link
%% --------------------------------------
ensure_deps_started() ->
    application:ensure_all_started(?PLATFORMLIB, permanent),
    application:ensure_all_started(?FSYNCLIB, permanent).
