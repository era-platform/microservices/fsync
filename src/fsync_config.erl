%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 02.08.2021
%%% @doc Configuration functions

-module(fsync_config).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    log_destination/1,
    sync_folder_path/0,
    max_user_watches/1,
    cooldown_timeout/0,
    history_deleted_keep_days/0,
    resync_interval/0,
    resync_immediately_after_fsync_result/0,
    force_sync_node/0,
    copier_bandwidth/0
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------
log_destination(Default) ->
    ?BU:get_env(?APP,'log_destination', Default).

%% --------------------
sync_folder_path() ->
    {ok,CWD} = file:get_cwd(),
    Default = filename:join(CWD,"sync_folder"),
    ?BU:get_env(?APP,'sync_folder_path', Default).

%% --------------------
max_user_watches(Default) ->
    ?BU:get_env(?APP,'max_user_watches', Default).

%% --------------------
cooldown_timeout() ->
    ?BU:get_env(?APP,'cooldown_timeout', 1000).

%% --------------------
history_deleted_keep_days() ->
    ?BU:get_env(?APP,'history_deleted_keep_days', 365).

%% --------------------
resync_interval() ->
    ?BU:get_env(?APP,'resync_interval', 60000).

%% --------------------
resync_immediately_after_fsync_result() ->
    ?BU:to_bool(?BU:get_env(?APP,'resync_immediately_after_fsync_result', true)).

%% --------------------
force_sync_node() ->
    ?BU:to_bool(?BU:get_env(?APP,'force_sync_node', true)).

%% --------------------
copier_bandwidth() ->
    ?BU:get_env(?APP,'copier_bandwidth', 65536000).

%% ====================================================================
%% Internal functions
%% ====================================================================