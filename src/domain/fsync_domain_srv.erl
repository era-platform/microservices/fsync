%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.05.2021
%%% @doc Facade fsync server.
%%%        Handles monitor requests
%%%        Perform internal checks
%%%      Opts:
%%%        regname, domain

-module(fsync_domain_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-record(dstate, {
    regname :: binary(),
    domain :: binary() | atom(),
    site :: binary(),
    self :: pid(),
    %
    fsynclib_regname :: atom(),
    fsynclib_pid :: pid(),
    %
    ref :: reference(),
    timerref :: reference()
}).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) when is_map(Opts) ->
    Domain = maps:get(domain,Opts),
    RegName = get_regname(Domain),
    gen_server:start_link({local, RegName}, ?MODULE, Opts#{'regname' => RegName}, []).

%% returns reg name of supv
get_regname(Domain) when is_binary(Domain); is_atom(Domain) ->
    ?BU:to_atom_new(?BU:strbin("fsync:srv;dom:~ts",[Domain])).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) when is_map(Opts) ->
    [RegName, Domain] = ?BU:extract_required_props(['regname','domain'], Opts),
    % ----
    Ref = make_ref(),
    gen_server:cast(self(),{'init',Ref}),
    % ----
    State = #dstate{regname = RegName,
                    domain = Domain,
                    site = ?PCFG:get_current_site(),
                    self = self(),
                    ref = Ref},
    % ----
    ?LOG('$info', "~ts. '~ts' facade inited", [?APP, Domain]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% --------------
%% returns current node
handle_call({get_node}, _From, State) ->
    {reply, {node(), self()}, State};

%% --------------
%% print state
handle_call({print}, _From, #dstate{domain=Domain}=State) ->
    ?LOG('$force', " ~ts. '~ts' facade state: ~n\t~120tp", [?APP, Domain, State]),
    {reply, ok, State};

%% --------------
%% restart gen_serv
handle_call({restart}, _From, State) ->
    {stop, restart, ok, State};

%% --------------
%% other
handle_call(_Msg, _From, #dstate{domain=Domain}=State) ->
    ?LOG('$trace', "~ts. '~ts' Unknown call: ~120tp", [?APP,Domain,_Msg]),
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({'init',Ref}, #dstate{ref=Ref}=State) ->
    State1 = start_domain(State),
    {noreply, State1};

%% --------------
%% other
handle_cast(_Msg, #dstate{domain=Domain}=State) ->
    ?LOG('$trace', "~ts. '~ts' Unknown cast: ~120tp", [?APP,Domain,_Msg]),
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% --------------
handle_info(_Info, #dstate{domain=Domain}=State) ->
    ?LOG('$trace', "~ts. '~ts' Unknown info: ~120tp", [?APP,Domain,_Info]),
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, #dstate{}=_State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% start self-domain supervisor of fsynclib
start_domain(#dstate{domain=Domain}=State) ->
    SrvRegName = ?FSYNCLIB:get_regname(Domain),
    Path = ?DU:build_path(Domain),
    Nodes = ?DU:get_nodes(Domain),
    Groups = ?DU:get_groups(Domain),
    ?BLfilelib:ensure_dir(filename:join(Path,".")),
    ChildSpec = ?FSYNCLIB:get_folder_childspec(SrvRegName, Domain, Path, Nodes, Groups),
    StartRes = ?DSUPV:start_child(Domain,ChildSpec),
    ?LOG('$trace', "'~ts'. Start fsynclib child: ~120tp", [Domain, StartRes]),
    % or (if it should be linked to dmlib's supv, but then deletion should be implemented)
    % Res = ?DMLIB:add_folder(Domain, Path, Nodes, Groups),
    % {ok,_SupvPid,SrvRegName} = Res,
    State#dstate{fsynclib_regname=SrvRegName,
                 fsynclib_pid=whereis(SrvRegName)}.